VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 1  'NoTransaction
END
Attribute VB_Name = "ClassFactory"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Windows NT Users: Reference the Microsoft Transaction Server Type Library
'Windows 2000 Users: Reference the COM+ Services Lib
'All Users: Make sure you have XML 3.0 and At least ADO 2.5 installed

Public Function DoRegistration(strXML As String, ByVal strProgID As String, _
                            Optional ByVal strConnection As String) As Boolean

On Error GoTo errHandler

Dim objRegister As IRegister
Dim objContext As ObjectContext

    'Don't fully qualify, since NT4 uses MTXAS dll, and 2000 uses ComSvcslib
    Set objContext = GetObjectContext()

    'Actually create the object we were requested to...
    Set objRegister = objContext.CreateInstance(strProgID)
    
    'Connection String is optional, since we may not have objects
    'That connect to the database.
    If Not IsMissing(strConnection) Then
        DoRegistration = objRegister.SignUp(strXML, strConnection)
    Else
        DoRegistration = objRegister.SignUp(strXML)
    End If
    
    Set objRegister = Nothing
    
    objContext.SetComplete
    
    'Don't forget to get rid of objContext!
    Set objContext = Nothing
    
    Exit Function
    
errHandler:
    'Here you can determine if we should write to event log,
    'raise to the user, or anything else you'd like
    
    DoRegistration = False
    objContext.SetAbort
    
    'Kill everything just in case...
    Set objContext = Nothing
    Set objRegister = Nothing
    
    Err.Raise Err.Number, Err.Source, Err.Description

End Function
