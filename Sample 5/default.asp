<%@  language="VBScript" %>
<%'Option Explicit%>

<!--#include file="Conexao.asp" -->
<!--#include file="clientes_dao.asp" -->
<!--#include file="DTO_clientes.asp" -->

<%

Dim counter
Dim objconn, daoclientes, novocliente

Call Main()

Sub Main
	
    'Cria o objeto de conexão e abre a conexão usando o método do objeto.
    Set objconn = new conexao
    objconn.openconn()

    'Cria o dao clientes para acesso às tabelas e inicializa a conexão no objeto.
    Set daoclientes = new clientes_dao
    Daoclientes.initialize(objconn.conn)

    'cria o novo cliente
    Set novocliente = new dto_clientes
    Novocliente.nome = "Carlos Alberto"

    'Insere o novo cliente no banco de dados e fecha a conexão
    Daoclientes.insert(novocliente)
    
    'objconn.closeconn()


    'Onde está a SQL? Embutida no código da classe. Desta forma, você passa a trabalhar apenas com objetos.
    'E para fazer a seleção? Basta usar o findall!
    'Cria o objeto de conexão e abre a conexão usando o método do objeto.
    
    'Set objconn = new conexao
    'objconn.openconn()

    'Cria o dao clientes para acesso às tabelas e inicializa a conexão no objeto.
    
    'Set daoclientes = new clientes_dao
    'Daoclientes.initialize(objconn.conn)

    'Seleciona todos os clientes do banco de dados e fecha a conexão
    Set lista = daoclientes.findall()
    

    For each item in lista.items
        Response.write "codigo:" & item.id &"<br>"
        Response.write "nome:" & item.nome &"<br>"
    next

    'E para atualizar? Da mesma forma! Basta utilizar os objetos:
    Set atualiza = new DTO_clientes
    Atualiza.id=8
    Atualiza.nome= "alberto roberto"
    daoclientes.update(atualiza)

    objconn.closeconn()

End Sub

%>
